# Grafana_backup

## Motivation to code this

There were two reasons to start coding grafana_backup.
The first one was the missing Grafana dashboard backup functionality. I'm collecting several weather data with a Raspi by help of Influxdb and presesenting them via Grafana. Influxdb provides backup mechanisms but self hosted Grafana doesn't.  
The second reasons was that I was interested to learn Rust. So the implementation is more or less a Rust self-training project.  
At the end it works, at least for me ;-).

## Short explanantion as provided by the --help option

```txt
grafana_backup 0.8.5
Backup your Grafana dashboards via API

You can import or restore the content of the *_gui.json files via the Grafana GUI. Via API one can
import the *_import.json files or restore the *_restore.json files e.g by help of 'curl'. Import means the dashboard isn't already existing. Restore means the dashboard aöready exists. For authentication a Grafana API key must be created and used as bearer option.

A curl example to import:

curl "http://localhost:3000/api/dashboards/import" -X POST -H "Accept: application/json" -H
"Content-Type: application/json" -H "Authorization: Bearer
eyJrIjoiMHRCQzdMMHM1WUZ3UzZTTW9scmVjN0pVVnVMcXZ1eXIiLCJuIjoidGVzdCIsImlkIjoxfQ==" -d
'@test_import.json'

USAGE:
    grafana_backup [OPTIONS] [SERVER]

ARGS:
    <SERVER>
            Grafana server socket address (IP address and port). This (or default) is used in any
            case, even if flag --file is set!
            
            [default: localhost:3000]

OPTIONS:
    -b, --bearer <BEARER>
            The authentication bearer string. Input in CLI '<bearer>'

    -d, --dir <DESTDIR>
            The backup root directory
            
            [default: ~/grafana_backup]

    -f, --file
            Use configuration from file. Parallel provided command line arguments are ignored!

    -h, --help
            Print help information

    -n, --dry-run
            Dry-run for backup

    -t, --tag <TAG>
            Filter backup by dashboard tag

    -v, --verbose
            Verbose output

    -V, --version
            Print version information

```

## Build, build targets and pre-build binaries

For your own builds install Rust, clone the repo and compile or cross-compile for the needed targets.  
I'm natively compiling on a Ubuntu 20.04 LTS installation for x86_64-unknown-linux-gnu and x86_64-unknown-linux-musl and cross-compiling for aarch64-unknown-linux-musl and aarch64-unknown-linux-gnu. The cross-compiled targets are used on my Raspi with a running Ubuntu 20.04 LTS 64 bit installation.
The installed Rust version is 1.59.0. All release targets are stripped.   
Grafana_backup is checked against the current Grafana release (just now version 8.4.3).   
Some grafana_backup binaries are provided in the releases directory.
