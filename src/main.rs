// For file and json handling
use anyhow::Context; //for error handling
use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};
use serde_json;
use std::fs::OpenOptions;
use std::fs;
use std::io::{Read, Write};
use std::net::ToSocketAddrs;
use std::path::PathBuf;
//For command line parsing
use clap::Parser;
//for logging
use log::*;

//use clap_verbosity_flag::Verbosity;

#[derive(Debug, Parser)]
#[clap(version)]
/// Backup your Grafana dashboards via API
///
/// You can import or restore the content
/// of the *_gui.json files via the Grafana GUI.
/// Via API one can import the *_import.json files or restore the *_restore.json files e.g by help of 'curl'.
/// Import means the dashboard isn't already existing. Restore means the dashboard aöready exists.
/// For authentication a Grafana API key must be created and used as bearer option.
///
/// A curl example to import:
///
/// curl "http://localhost:3000/api/dashboards/import"
///  -X POST
///  -H "Accept: application/json"
///  -H "Content-Type: application/json"
///  -H "Authorization: Bearer eyJrIjoiMHRCQzdMMHM1WUZ3UzZTTW9scmVjN0pVVnVMcXZ1eXIiLCJuIjoidGVzdCIsImlkIjoxfQ=="
///  -d '@test_import.json'

struct Cli {
    //Args
    /// Grafana server socket address (IP address and port). This (or default) is used in any case, even if flag --file is set!
    #[clap(default_value = "localhost:3000")]
    server: String,

    /// The backup root directory.
    #[clap(long = "dir", short = 'd', default_value = "~/grafana_backup")]
    destdir: String,

    //Options
    /// The authentication bearer string. Input in CLI '<bearer>'.
    #[clap(short, long)]
    bearer: Option<String>,
    ///Filter backup by dashboard tag.
    #[clap(short, long)]
    tag: Option<String>,

    //Flags
    /// Dry-run for backup
    #[clap(short = 'n', long = "dry-run")]
    dry_run: bool,

    /// Verbose output
    #[clap(short, long)]
    verbose: bool,

    ///Use configuration from file. Parallel provided command line arguments are ignored!
    #[clap(short, long)]
    file: bool,
}

#[derive(Debug, Serialize, Deserialize)]
struct Save {
    //Args
    server: String,
    destdir: String,

    //Options
    bearer: Option<String>,
    tag: Option<String>,
}


fn main() -> anyhow::Result<()> {
    let cli = Cli::parse(); //Load config from CLI
    let ver;
    if cli.verbose {
        ver = 2; //allow warn and info
    } else {
        ver = 0; //not verbose
    }
    stderrlog::new()
        .module(module_path!())
        .timestamp(stderrlog::Timestamp::Second)
        .verbosity(ver)
        .init()
        .unwrap();

    //construct the struct for save in config file
    let mut cli_save = Save {
        server: cli.server.clone(),
        destdir: cli.destdir.clone(),
        bearer: cli.bearer.clone(),
        tag: cli.tag.clone(),
    };

    //Check if server address is a valid address
    let mut _server_addr_res = cli
        .server
        .to_socket_addrs()
        .with_context(|| format!("Invalid server socket address: {:?}!", cli.server))?;
    let server_addr_re = /*sock_addr_to_string(*/&_server_addr_res.next().unwrap().to_string();//);
    info!(
        "Grafana server socket adddress set to {:#?}.",
        &server_addr_re
    );

    // Make config file dir structure
    let mut config_dir =
        dirs::config_dir().with_context(|| format!("User config dir not found!"))?; //users config dir
    config_dir.push("grafana_backup");

    if !cli.dry_run {
        fs::create_dir_all(&config_dir)
            .with_context(|| format!("Unable to open '{}'!", config_dir.to_string_lossy()))?; //create dir
        info!("Config file directory {:#?} created.", &config_dir);
    } else {
        warn!(
            "Dry_run flag set: config file directory {:#?} prepared but not created!",
            &config_dir
        );
    }
    config_dir.push("db_backup_".to_string() + &cli.server + ".toml"); //construct config file name
                                                                       // read config file if --file flag is set. Command line input will be overwtitten
    if cli.file {
        let mut file = OpenOptions::new()
            .read(true)
            .open(&config_dir)
            .with_context(|| {
                format!(
                    "Config file {} couldn't be opened!",
                    config_dir.to_string_lossy()
                )
            })?;
        let mut toml_buf = String::new();
        file.read_to_string(&mut toml_buf).with_context(|| {
            format!(
                "Config file {} couldn't be read!",
                config_dir.to_string_lossy()
            )
        })?;   

        cli_save = toml::from_str(&toml_buf).with_context(|| {
            format!(
                "Config file content not toml formatted!"
            )
        })?;   

        info!("Flag -f set, config file {:#?} read.", &config_dir);
    }
    if cli.dry_run {
        warn!(
            "Dry_run flag set: config file  {:#?} not written!",
            &config_dir
        );
    }
    //Write config file if --file and --dry-run flags aren't set
    if !cli.file && !cli.dry_run {
        //Save config file as json
        write_toml_to_file(
            &config_dir,
            &toml::to_string_pretty(&cli_save)
                .with_context(|| format!("Unable to convert config data to toml!"))?,
        )
        .with_context(|| {
            format!(
                "Unable to save config file {}!",
                config_dir.to_string_lossy()
            )
        })?;
        info!(r#"Config file {:#?} written."#, &config_dir);
    }


    // Make backup dir structure
    let p = shellexpand::tilde(&cli.destdir); //substitute ~ in case of default dir for backup
    let mut dir = PathBuf::from(p.as_ref()); //Convert backup dir to pathbuf
    dir.push(&cli.server); //Append server sub-dir
    let now: DateTime<Local> = Local::now();
    let subdir = now.format("%Y-%m-%d_%H:%M").to_string();
    dir.push(subdir); //Append date time sub-dir
    if !cli.dry_run {
        fs::create_dir_all(&dir).with_context(|| {
            format!("unable to create backup dir {:#?}!", dir.to_string_lossy())
        })?; //  //Create backup dir path
        info!("Backup directory {:#?} created.", &dir);
    } else {
        warn!(
            "Dry_run flag set: backup directory {:#?} prepared but not created!",
            &dir
        );
    }
    //Get dashboard definition and save it to file
    //Address and tag as Parameter
    let mut server_addr = "http://".to_string() + &server_addr_re + "/api/search?";
    if cli.tag != None {
        server_addr = server_addr + "tag=" + &cli.tag.unwrap();
    }
    //Bearer as parameter
    let db_def: serde_json::Value;
    db_def = read_json_from_url_wb(&server_addr, &cli_save.bearer.as_ref()).with_context(|| {
        format!(
            "Can't get Grafana json dashboard definition from {}!",
            server_addr
        )
    })?;

    info!(
        "Grafana dashboard definition loaded from url: {:#?}.",
        &server_addr
    );
    //Write object description file if --dry_run flag isn't set
    dir.push("db_object_desc.json");
    if !cli.dry_run {
        //Write object description file
        write_json_to_file(&dir, &db_def).with_context(|| {
            format!(
                "Unable to save object description file {}!",
                dir.to_string_lossy()
            )
        })?;
        info!("Grafana dashboard definition saved to {:#?}.", &dir);
    } else {
        warn!(
            "Dry_run flag set: Grafana dashboard definition not saved to {:#?}!",
            &dir
        );
    }
    dir.pop(); //Unload file name
    let number_objects = db_def
        .as_array()
        .with_context(|| format!("Json formatted Grafana dashboard definition isn't an array!"))?
        .len();
    info!("{:#?} Grafana dashboard objects found.", number_objects);
    let mut db_objects = Vec::new(); //Vector of tuple of dashboard objects names and uid's
    let mut db_content: serde_json::Value;
    for n in 0..number_objects {
        db_objects.push((
            db_def[n]["title"]
                .as_str()
                .with_context(|| format!("No title for a dashboard object!"))?,
            db_def[n]["uid"]
                .as_str()
                .with_context(|| format!("No uid for a dashboard object!"))?,
        ));

        //Read db url
        let db_addr =
            "http://".to_string() + &server_addr_re + "/api/dashboards/uid/" + db_objects[n].1;
        db_content = read_json_from_url_wb(&db_addr, &cli_save.bearer.as_ref())
            .with_context(|| format!("Can't get Grafana json dashboard from {}!", &db_addr))?;
        info!(
            "Read dashboard {:#?} json with uid {:#?} from url {:#?}.",
            db_objects[n].0, db_objects[n].1, &db_addr
        );

        //json manipulations
        //remove meta key
        let mut map = db_content.as_object_mut();
        if map.is_none() {
            error!(
                "Loaded dashboard for {} has no json map!\n Backup not saved for this dashboard",
                db_objects[n].0
            );
            continue;
        } else {
            if map.unwrap().remove_entry("meta").is_none() {
                error!("Loaded dashboard for {} has no meta data!\n Backup nevertheless continued for this dashboard", db_objects[n].0);
            }
        }
        //write to file for restore via API
        dir.push(db_objects[n].0.to_string() + "_restore.json");

        if !cli.dry_run {
            //Write dashboard file
            write_json_to_file(&dir, &db_content).with_context(|| {
                format!(
                    "Unable to save dashboard json file: {}!",
                    dir.to_string_lossy()
                )
            })?;
            info!(
                "For dashboard {:#?} json data manipulated. \nPrepared for restore via API and saved as {:#?}.",
                db_objects[n].0, &dir
            );
        } else {
            warn!(
                "Dry_run flag set: For dashboard {:#?} json data manipulated. \nPrepared for restore via API but not saved!",
                db_objects[n].0
            );
        }
        dir.pop(); //Unload file name
                   //Set uid and id to Null if exists
        db_content
            .pointer_mut("/dashboard/uid")
            .map(|v| *v = serde_json::Value::Null); //"".into());

        db_content
            .pointer_mut("/dashboard/id")
            .map(|v| *v = serde_json::Value::Null);

        //write to file for import via API
        dir.push(db_objects[n].0.to_string() + "_import.json");

        if !cli.dry_run {
            //Write dashboard file
            write_json_to_file(&dir, &db_content).with_context(|| {
                format!(
                    "Unable to save dashboard json file: {}!",
                    dir.to_string_lossy()
                )
            })?;
            info!(
                "For dashboard {:#?} json data manipulated. \nPrepared for import via API and saved as {:#?}.",
                db_objects[n].0, &dir
            );
        } else {
            warn!(
                "Dry_run flag set: For dashboard {:#?} json data manipulated. \nPrepared for import via API but not saved!",
                db_objects[n].0
            );
        }
        dir.pop(); //Unload file name

        //remove the "dashboard" key frame if prepared for GUI import

        map = db_content.as_object_mut();
        let content = map.unwrap().remove("dashboard");
        if content.is_some() {
            db_content
                .as_object_mut()
                .unwrap()
                .append(content.unwrap().as_object_mut().unwrap());
        } else {
            error!("Loaded dashboard for {} has no json dashboard object!\n Backup nevertheless continued for this dashboard", db_objects[n].0);
        }

        //write to file for GUI import/restore
        dir.push(db_objects[n].0.to_string() + "_gui.json");

        if !cli.dry_run {
            //Write dashboard file
            write_json_to_file(&dir, &db_content).with_context(|| {
                format!(
                    "Unable to save dashboard json file: {}!",
                    dir.to_string_lossy()
                )
            })?;
            info!(
                "For dashboard {:#?} json data manipulated. \nPrepared for import or restore via GUI and saved as {:#?}.",
                db_objects[n].0, &dir
            );
        } else {
            warn!(
                "Dry_run flag set: For dashboard {:#?} json data manipulated. \nPrepared for import or restore via GUI but not saved!",
                db_objects[n].0
            );
        }
        dir.pop(); //Unload file name
    }
    Ok(())
}

//function to  write json buffer to file
fn write_json_to_file(filename: &PathBuf, buffer: &serde_json::Value) -> anyhow::Result<()> {
    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(&filename)?;
    serde_json::to_writer_pretty(&file, &buffer)?;

    Ok(())
}

//function to  write toml buffer to file
fn write_toml_to_file(filename: &PathBuf, buffer: &String) -> anyhow::Result<()> {
    let mut file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(&filename)?;
    file.write_all(buffer.as_bytes())?;

    Ok(())
}

//function to read url to json buffer

fn read_json_from_url_wb(
    url: &String,
    bearer: &Option<&String>,
) -> anyhow::Result<serde_json::Value, anyhow::Error> {
    let auth: String;
    if bearer.is_some() {
        auth = "Bearer ".to_string() + bearer.unwrap();
    } else {
        auth = "".to_string();
    }

    let value: serde_json::Value = ureq::get(&url)
        .set("Authorization", &auth)
        .set("Accept", "application/json")
        .set("Content-Type", "application/json")
        .call()?
        .into_json()?;
    Ok(value)
}
